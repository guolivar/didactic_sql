-- Get all the datapoints with their associated route name
-- matched both by expected route and location within that route
-- Output is timestamp, measured value and route name.
-- md.geom would add a geometry column to use in a GIS
SELECT distinct md.recordtime at time zone 'NZST'as recordtime, md.value as n20, b.name as route
FROM admin.instruments i, admin.sensors s, admin.blocks b, data.mobilemeasurements mm, data.mobiledata md
WHERE i.name='PTrak' --The instrument to use
  AND s.instrumentid=i.id
  AND s.name='N20' --The measurement to use
  AND mm.instrumentid=i.id
  AND md.sensorid=s.id
  AND md.sensortime=mm.gpsdate
  AND mm.blockid=b.id
  AND ST_Within(mm.geom, ST_Buffer((select geom from admin.blocks where name=b.name), 0.0005));