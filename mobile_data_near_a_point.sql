-- Select data from the mobile dataset that was located (via GPS), within 50m of
-- a given point (in the example, the fixed site at Queen Street)
select nmd.value as n20, nmd.recordtime at time zone 'NZST' as recordtime  
from data.mobilemeasurements nmm, data.mobiledata nmd, admin.sensors ns, admin.instruments ni
where ni.name='PTrak' --Instrument to use
  AND ns.instrumentid=ni.id
  AND ns.name='N20' --Measurement to use
  AND nmm.instrumentid=ni.id
  AND nmd.sensorid=ns.id
  AND nmd.sensortime=nmm.gpsdate
  AND ST_DWithin(nmm.geom, (select geom from admin.fixedsites where name='Queen Street'), 0.0005); --This is an approximation of 50m at 35 latitude South