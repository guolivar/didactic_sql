-- Summary of the measurements taken, grouped by instrument and selected both
-- by Route LABEL and position within X metres of the actual route
-- summary functions are average, min, max, stdeviation and count
-----------------------------------------
-- First select the output. Summary functions and route name.
SELECT distinct count(md.value) as nrecs, avg(md.value) as av, max(md.value) as mx, min(md.value) as mn, i.serialn as instrument
-- Then identify whic tables to use to select the data.
-- The syntax is <table> <alias> and the alias is to simplify the calls below
FROM admin.instruments i, admin.sensors s, admin.blocks b, data.mobilemeasurements mm, data.mobiledata md
-- Now come the actual conditions to match the tables and select the data.
WHERE i.name='Langan' -- Instrument name
  AND md.flagid=1
  AND s.instrumentid=i.id -- Match the instruments and sensors tables
  AND s.name='CO' -- Sensor name
  AND mm.instrumentid=i.id -- Match the instrument with the measurement point
  AND md.sensorid=s.id -- Match the sensor with the measurement data
  AND md.recordtime=mm.recordtime -- Match the GPS time with the sensor time
  AND mm.blockid=b.id -- Match the block from the measurements with the route
  AND ST_Within(mm.geom, ST_Buffer((select geom from admin.blocks where name=b.name), 0.0005)) -- Select only points that are INSIDE a 50m buffer around the shape of the routes (as polygons)
-- Finally, group the results by the instrument serial number (i.e, only output one row per instrument)
group by i.serialn;

