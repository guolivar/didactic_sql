The scripts in this repository are always a work in progress and as such they must always be reviewed before using.
Note that the scripts under ADMIN may require SUPERUSER credentials as they can potentially alter the schemas and the functionality of the database.
