-- UPDATE data.mobiledata
-- SET flagid=2
-- WHERE sensorid in (SELECT s.id from admin.sensors as s, admin.instruments as i 
-- 			where s.instrumentid=i.id
-- 			AND i.name in ('Langan','PTrak'));
-- 			
insert into data.mobiledata
(sensorid,value,sensortime,recordtime,flagid)
select md.sensorid, md.value*1.024 as value, md.sensortime, md.recordtime, 3 as flagid
from data.mobiledata md, admin.instruments i, admin.sensors s
where md.sensorid=s.id
and s.name='N20'
and s.instrumentid=i.id
and i.notes like 'P4%';


