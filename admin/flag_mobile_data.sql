UPDATE data.mobiledata
SET flagid=3
WHERE sensorid in (SELECT s.id from admin.sensors as s, admin.instruments as i 
			where s.instrumentid=i.id
			AND i.name='Langan'
			AND s.name='Temperature');