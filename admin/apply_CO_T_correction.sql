insert into data.mobiledata
(sensorid,sensortime,recordtime,value,flagid)
select co_temp.sensorid, co_temp.sensortime, co_temp.recordtime, ((co_temp.co*100-(1.75^((co_temp.temp-20)/10)))/(1+(co_temp.temp-20)*0.0055)) as value, 1 as flagid
from data.mobilemeasurements mm,
(select distinct co.sensorid, co.sensortime, co.recordtime, co.value as co, tmp.value as temp, co.instrumentid as instrumentid
from 
(select sd1.recordtime, sd1.value, adi1.id as instrumentid from data.mobiledata sd1,admin.sensors ads1, admin.instruments adi1
  where ads1.instrumentid=adi1.id
  and sd1.sensorid=ads1.id
  and sd1.flagid=3
  and ads1.name='Temperature'
  and sd1.value<20) tmp,
(select sd2.sensorid, sd2.recordtime, sd2.value, sd2.sensortime, adi2.id as instrumentid from data.mobiledata sd2,admin.sensors ads2, admin.instruments adi2
  where ads2.instrumentid=adi2.id
  and sd2.sensorid=ads2.id
  and sd2.flagid=2
  and ads2.name='CO') co
where co.recordtime=tmp.recordtime
and co.instrumentid=tmp.instrumentid) co_temp;