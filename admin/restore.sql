--
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = data, pg_catalog;

ALTER TABLE ONLY data.mobilemeasurements DROP CONSTRAINT mobilemeasurements_peopleid_fkey;
ALTER TABLE ONLY data.mobilemeasurements DROP CONSTRAINT mobilemeasurements_instrumentid;
ALTER TABLE ONLY data.mobilemeasurements DROP CONSTRAINT mobilemeasurements_blockid_fkey;
ALTER TABLE ONLY data.mobiledata DROP CONSTRAINT mobiledata_sensor_fkey;
ALTER TABLE ONLY data.metadata DROP CONSTRAINT metadata_peopleid;
ALTER TABLE ONLY data.metadata DROP CONSTRAINT metadata_instrumentid;
ALTER TABLE ONLY data.metadata DROP CONSTRAINT metadata_blockid;
ALTER TABLE ONLY data.fixedmeasurements DROP CONSTRAINT fixedmeasurements_siteid_fkey;
ALTER TABLE ONLY data.fixedmeasurements DROP CONSTRAINT fixedmeasurements_parameterid_fkey;
SET search_path = admin, pg_catalog;

ALTER TABLE ONLY admin.sensors DROP CONSTRAINT sensors_instrumentid_fkey;
ALTER TABLE ONLY admin.people DROP CONSTRAINT people_affiliation_fkey;
ALTER TABLE ONLY admin.instruments DROP CONSTRAINT instruments_foreign_key_owner;
ALTER TABLE ONLY admin.fixedsites DROP CONSTRAINT fixedsites_ownerid_fkey;
SET search_path = data, pg_catalog;

DROP INDEX data.mobilemeasurements_year_idx;
DROP INDEX data.mobilemeasurements_seconds_idx;
DROP INDEX data.mobilemeasurements_recordtime_idx;
DROP INDEX data.mobilemeasurements_peopleid_idx;
DROP INDEX data.mobilemeasurements_month_idx;
DROP INDEX data.mobilemeasurements_minute_idx;
DROP INDEX data.mobilemeasurements_instrumentid_idx;
DROP INDEX data.mobilemeasurements_hour_idx;
DROP INDEX data.mobilemeasurements_gpsdate_idx;
DROP INDEX data.mobilemeasurements_geom_gix;
DROP INDEX data.mobilemeasurements_day_idx;
DROP INDEX data.mobilemeasurements_blockid_idx;
DROP INDEX data.mobiledata_sensortime_idx;
DROP INDEX data.mobiledata_sensorid_idx;
DROP INDEX data.mobiledata_recordtime_idx;
SET search_path = admin, pg_catalog;

DROP INDEX admin.peopleidx1;
SET search_path = data, pg_catalog;

ALTER TABLE ONLY data.mobilemeasurements DROP CONSTRAINT mobilemeasurements_pkey;
ALTER TABLE ONLY data.mobiledata DROP CONSTRAINT mobiledata_pkey;
ALTER TABLE ONLY data.metadata DROP CONSTRAINT metadata_pkey;
ALTER TABLE ONLY data.fixedmeasurements DROP CONSTRAINT fixedmeasurements_pkey;
SET search_path = admin, pg_catalog;

ALTER TABLE ONLY admin.sensors DROP CONSTRAINT sensors_pkey;
ALTER TABLE ONLY admin.people DROP CONSTRAINT people_pkey;
ALTER TABLE ONLY admin.instruments DROP CONSTRAINT instruments_serialn_key;
ALTER TABLE ONLY admin.instruments DROP CONSTRAINT instruments_pkey;
ALTER TABLE ONLY admin.institutions DROP CONSTRAINT institutions_pkey;
ALTER TABLE ONLY admin.fixedsites DROP CONSTRAINT fixedsites_pkey;
ALTER TABLE ONLY admin.blocks DROP CONSTRAINT blocks_pkey;
SET search_path = data, pg_catalog;

ALTER TABLE data.mobilemeasurements ALTER COLUMN id DROP DEFAULT;
ALTER TABLE data.mobiledata ALTER COLUMN id DROP DEFAULT;
ALTER TABLE data.metadata ALTER COLUMN id DROP DEFAULT;
ALTER TABLE data.fixedmeasurements ALTER COLUMN id DROP DEFAULT;
SET search_path = admin, pg_catalog;

ALTER TABLE admin.sensors ALTER COLUMN id DROP DEFAULT;
ALTER TABLE admin.people ALTER COLUMN id DROP DEFAULT;
ALTER TABLE admin.instruments ALTER COLUMN id DROP DEFAULT;
ALTER TABLE admin.institutions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE admin.fixedsites ALTER COLUMN id DROP DEFAULT;
ALTER TABLE admin.blocks ALTER COLUMN id DROP DEFAULT;
SET search_path = data, pg_catalog;

DROP SEQUENCE data.mobilemeasurements_id_seq;
DROP TABLE data.mobilemeasurements;
DROP SEQUENCE data.mobiledata_id_seq;
DROP TABLE data.mobiledata;
DROP SEQUENCE data.metadata_id_seq;
DROP TABLE data.metadata;
DROP SEQUENCE data.fixedmeasurements_id_seq;
DROP TABLE data.fixedmeasurements;
SET search_path = admin, pg_catalog;

DROP SEQUENCE admin.sensors_id_seq;
DROP TABLE admin.sensors;
DROP SEQUENCE admin.people_id_seq;
DROP TABLE admin.people;
DROP SEQUENCE admin.instruments_id_seq;
DROP TABLE admin.instruments;
DROP SEQUENCE admin.institutions_id_seq;
DROP TABLE admin.institutions;
DROP SEQUENCE admin.fixedsites_id_seq;
DROP TABLE admin.fixedsites;
DROP SEQUENCE admin.blocks_id_seq;
DROP TABLE admin.blocks;
DROP SCHEMA public;
DROP SCHEMA metadata;
DROP SCHEMA gis_views;
DROP SCHEMA data;
DROP SCHEMA admin;
--
-- Name: admin; Type: SCHEMA; Schema: -; Owner: adm_penap
--

CREATE SCHEMA admin;


ALTER SCHEMA admin OWNER TO adm_penap;

--
-- Name: data; Type: SCHEMA; Schema: -; Owner: adm_penap
--

CREATE SCHEMA data;


ALTER SCHEMA data OWNER TO adm_penap;

--
-- Name: gis_views; Type: SCHEMA; Schema: -; Owner: adm_penap
--

CREATE SCHEMA gis_views;


ALTER SCHEMA gis_views OWNER TO adm_penap;

--
-- Name: metadata; Type: SCHEMA; Schema: -; Owner: adm_penap
--

CREATE SCHEMA metadata;


ALTER SCHEMA metadata OWNER TO adm_penap;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET search_path = admin, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: blocks; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE blocks (
    id integer NOT NULL,
    name text,
    purpose text NOT NULL,
    notes text,
    geom public.geometry NOT NULL
);


ALTER TABLE admin.blocks OWNER TO adm_penap;

--
-- Name: TABLE blocks; Type: COMMENT; Schema: admin; Owner: adm_penap
--

COMMENT ON TABLE blocks IS 'Route/block/area of study per deployment plan';


--
-- Name: blocks_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.blocks_id_seq OWNER TO adm_penap;

--
-- Name: blocks_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE blocks_id_seq OWNED BY blocks.id;


--
-- Name: fixedsites; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE fixedsites (
    id integer NOT NULL,
    name text,
    type text,
    ownerid integer,
    notes text,
    geom public.geometry NOT NULL
);


ALTER TABLE admin.fixedsites OWNER TO adm_penap;

--
-- Name: fixedsites_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE fixedsites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.fixedsites_id_seq OWNER TO adm_penap;

--
-- Name: fixedsites_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE fixedsites_id_seq OWNED BY fixedsites.id;


--
-- Name: institutions; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE institutions (
    id integer NOT NULL,
    name text,
    notes text
);


ALTER TABLE admin.institutions OWNER TO adm_penap;

--
-- Name: institutions_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE institutions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.institutions_id_seq OWNER TO adm_penap;

--
-- Name: institutions_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE institutions_id_seq OWNED BY institutions.id;


--
-- Name: instruments; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE instruments (
    id integer NOT NULL,
    serialn text,
    name text NOT NULL,
    notes text,
    owner integer
);


ALTER TABLE admin.instruments OWNER TO adm_penap;

--
-- Name: instruments_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE instruments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.instruments_id_seq OWNER TO adm_penap;

--
-- Name: instruments_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE instruments_id_seq OWNED BY instruments.id;


--
-- Name: people; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE people (
    id integer NOT NULL,
    name text NOT NULL,
    affiliation integer,
    role text
);


ALTER TABLE admin.people OWNER TO adm_penap;

--
-- Name: people_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE people_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.people_id_seq OWNER TO adm_penap;

--
-- Name: people_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE people_id_seq OWNED BY people.id;


--
-- Name: sensors; Type: TABLE; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE TABLE sensors (
    id integer NOT NULL,
    name text NOT NULL,
    units text,
    instrumentid integer
);


ALTER TABLE admin.sensors OWNER TO adm_penap;

--
-- Name: sensors_id_seq; Type: SEQUENCE; Schema: admin; Owner: adm_penap
--

CREATE SEQUENCE sensors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.sensors_id_seq OWNER TO adm_penap;

--
-- Name: sensors_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: adm_penap
--

ALTER SEQUENCE sensors_id_seq OWNED BY sensors.id;


SET search_path = data, pg_catalog;

--
-- Name: fixedmeasurements; Type: TABLE; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE TABLE fixedmeasurements (
    year integer,
    month integer,
    day integer,
    hour integer,
    minute integer,
    second numeric,
    id bigint NOT NULL,
    parameterid integer,
    siteid integer,
    recordtime timestamp with time zone,
    value text
);


ALTER TABLE data.fixedmeasurements OWNER TO adm_penap;

--
-- Name: fixedmeasurements_id_seq; Type: SEQUENCE; Schema: data; Owner: adm_penap
--

CREATE SEQUENCE fixedmeasurements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.fixedmeasurements_id_seq OWNER TO adm_penap;

--
-- Name: fixedmeasurements_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: adm_penap
--

ALTER SEQUENCE fixedmeasurements_id_seq OWNED BY fixedmeasurements.id;


--
-- Name: metadata; Type: TABLE; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE TABLE metadata (
    id integer NOT NULL,
    blockid integer,
    instrumentid integer,
    peopleid integer,
    date timestamp with time zone,
    type text,
    starttime timestamp with time zone,
    endtime timestamp with time zone,
    notes text
);


ALTER TABLE data.metadata OWNER TO adm_penap;

--
-- Name: TABLE metadata; Type: COMMENT; Schema: data; Owner: adm_penap
--

COMMENT ON TABLE metadata IS 'Metadata associated with the data incorporated into the database.';


--
-- Name: metadata_id_seq; Type: SEQUENCE; Schema: data; Owner: adm_penap
--

CREATE SEQUENCE metadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.metadata_id_seq OWNER TO adm_penap;

--
-- Name: metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: adm_penap
--

ALTER SEQUENCE metadata_id_seq OWNED BY metadata.id;


--
-- Name: mobiledata; Type: TABLE; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE TABLE mobiledata (
    id bigint NOT NULL,
    sensorid integer,
    value numeric,
    sensortime numeric,
    recordtime timestamp with time zone
);


ALTER TABLE data.mobiledata OWNER TO adm_penap;

--
-- Name: COLUMN mobiledata.sensortime; Type: COMMENT; Schema: data; Owner: adm_penap
--

COMMENT ON COLUMN mobiledata.sensortime IS 'Instrument time
Same instrument time for all the sensors on the instrument
In OCTAVE DATENUM format';


--
-- Name: mobiledata_id_seq; Type: SEQUENCE; Schema: data; Owner: adm_penap
--

CREATE SEQUENCE mobiledata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.mobiledata_id_seq OWNER TO adm_penap;

--
-- Name: mobiledata_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: adm_penap
--

ALTER SEQUENCE mobiledata_id_seq OWNED BY mobiledata.id;


--
-- Name: mobilemeasurements; Type: TABLE; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE TABLE mobilemeasurements (
    year integer,
    month integer,
    day integer,
    hour integer,
    minute integer,
    seconds numeric,
    id bigint NOT NULL,
    lat numeric NOT NULL,
    lon numeric NOT NULL,
    elevation numeric,
    gpsdate numeric NOT NULL,
    peopleid integer,
    instrumentid integer,
    blockid integer,
    geom public.geometry(Point,4326),
    recordtime timestamp with time zone
);


ALTER TABLE data.mobilemeasurements OWNER TO adm_penap;

--
-- Name: TABLE mobilemeasurements; Type: COMMENT; Schema: data; Owner: adm_penap
--

COMMENT ON TABLE mobilemeasurements IS 'The GPS gives me location and record for this table.
Each record is also attached to the Instrument IDs that were used
<position_info><instrumentID1>
<position_info><instrumentID2>
<position_info><instrumentID3>
<position_info><instrumentID4>';


--
-- Name: mobilemeasurements_id_seq; Type: SEQUENCE; Schema: data; Owner: adm_penap
--

CREATE SEQUENCE mobilemeasurements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data.mobilemeasurements_id_seq OWNER TO adm_penap;

--
-- Name: mobilemeasurements_id_seq; Type: SEQUENCE OWNED BY; Schema: data; Owner: adm_penap
--

ALTER SEQUENCE mobilemeasurements_id_seq OWNED BY mobilemeasurements.id;


SET search_path = admin, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY blocks ALTER COLUMN id SET DEFAULT nextval('blocks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY fixedsites ALTER COLUMN id SET DEFAULT nextval('fixedsites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY institutions ALTER COLUMN id SET DEFAULT nextval('institutions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY instruments ALTER COLUMN id SET DEFAULT nextval('instruments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY people ALTER COLUMN id SET DEFAULT nextval('people_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY sensors ALTER COLUMN id SET DEFAULT nextval('sensors_id_seq'::regclass);


SET search_path = data, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY fixedmeasurements ALTER COLUMN id SET DEFAULT nextval('fixedmeasurements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY metadata ALTER COLUMN id SET DEFAULT nextval('metadata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobiledata ALTER COLUMN id SET DEFAULT nextval('mobiledata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobilemeasurements ALTER COLUMN id SET DEFAULT nextval('mobilemeasurements_id_seq'::regclass);


SET search_path = admin, pg_catalog;

--
-- Name: blocks_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY blocks
    ADD CONSTRAINT blocks_pkey PRIMARY KEY (id);


--
-- Name: fixedsites_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY fixedsites
    ADD CONSTRAINT fixedsites_pkey PRIMARY KEY (id);


--
-- Name: institutions_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY institutions
    ADD CONSTRAINT institutions_pkey PRIMARY KEY (id);


--
-- Name: instruments_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_pkey PRIMARY KEY (id);


--
-- Name: instruments_serialn_key; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_serialn_key UNIQUE (serialn);


--
-- Name: people_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: sensors_pkey; Type: CONSTRAINT; Schema: admin; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY sensors
    ADD CONSTRAINT sensors_pkey PRIMARY KEY (id);


SET search_path = data, pg_catalog;

--
-- Name: fixedmeasurements_pkey; Type: CONSTRAINT; Schema: data; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY fixedmeasurements
    ADD CONSTRAINT fixedmeasurements_pkey PRIMARY KEY (id);


--
-- Name: metadata_pkey; Type: CONSTRAINT; Schema: data; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- Name: mobiledata_pkey; Type: CONSTRAINT; Schema: data; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY mobiledata
    ADD CONSTRAINT mobiledata_pkey PRIMARY KEY (id);


--
-- Name: mobilemeasurements_pkey; Type: CONSTRAINT; Schema: data; Owner: adm_penap; Tablespace: 
--

ALTER TABLE ONLY mobilemeasurements
    ADD CONSTRAINT mobilemeasurements_pkey PRIMARY KEY (id);


SET search_path = admin, pg_catalog;

--
-- Name: peopleidx1; Type: INDEX; Schema: admin; Owner: adm_penap; Tablespace: 
--

CREATE INDEX peopleidx1 ON people USING btree (id);


SET search_path = data, pg_catalog;

--
-- Name: mobiledata_recordtime_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobiledata_recordtime_idx ON mobiledata USING btree (recordtime);


--
-- Name: mobiledata_sensorid_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobiledata_sensorid_idx ON mobiledata USING btree (sensorid);


--
-- Name: mobiledata_sensortime_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobiledata_sensortime_idx ON mobiledata USING btree (sensortime);


--
-- Name: mobilemeasurements_blockid_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_blockid_idx ON mobilemeasurements USING btree (blockid);


--
-- Name: mobilemeasurements_day_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_day_idx ON mobilemeasurements USING btree (day);


--
-- Name: mobilemeasurements_geom_gix; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_geom_gix ON mobilemeasurements USING gist (geom);


--
-- Name: mobilemeasurements_gpsdate_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_gpsdate_idx ON mobilemeasurements USING btree (gpsdate);


--
-- Name: mobilemeasurements_hour_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_hour_idx ON mobilemeasurements USING btree (hour);


--
-- Name: mobilemeasurements_instrumentid_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_instrumentid_idx ON mobilemeasurements USING btree (instrumentid);


--
-- Name: mobilemeasurements_minute_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_minute_idx ON mobilemeasurements USING btree (minute);


--
-- Name: mobilemeasurements_month_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_month_idx ON mobilemeasurements USING btree (month);


--
-- Name: mobilemeasurements_peopleid_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_peopleid_idx ON mobilemeasurements USING btree (peopleid);


--
-- Name: mobilemeasurements_recordtime_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_recordtime_idx ON mobilemeasurements USING btree (recordtime);


--
-- Name: mobilemeasurements_seconds_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_seconds_idx ON mobilemeasurements USING btree (seconds);


--
-- Name: mobilemeasurements_year_idx; Type: INDEX; Schema: data; Owner: adm_penap; Tablespace: 
--

CREATE INDEX mobilemeasurements_year_idx ON mobilemeasurements USING btree (year);


SET search_path = admin, pg_catalog;

--
-- Name: fixedsites_ownerid_fkey; Type: FK CONSTRAINT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY fixedsites
    ADD CONSTRAINT fixedsites_ownerid_fkey FOREIGN KEY (ownerid) REFERENCES institutions(id);


--
-- Name: instruments_foreign_key_owner; Type: FK CONSTRAINT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY instruments
    ADD CONSTRAINT instruments_foreign_key_owner FOREIGN KEY (owner) REFERENCES institutions(id) MATCH FULL;


--
-- Name: people_affiliation_fkey; Type: FK CONSTRAINT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY people
    ADD CONSTRAINT people_affiliation_fkey FOREIGN KEY (affiliation) REFERENCES institutions(id);


--
-- Name: sensors_instrumentid_fkey; Type: FK CONSTRAINT; Schema: admin; Owner: adm_penap
--

ALTER TABLE ONLY sensors
    ADD CONSTRAINT sensors_instrumentid_fkey FOREIGN KEY (instrumentid) REFERENCES instruments(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


SET search_path = data, pg_catalog;

--
-- Name: fixedmeasurements_parameterid_fkey; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY fixedmeasurements
    ADD CONSTRAINT fixedmeasurements_parameterid_fkey FOREIGN KEY (parameterid) REFERENCES admin.sensors(id);


--
-- Name: fixedmeasurements_siteid_fkey; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY fixedmeasurements
    ADD CONSTRAINT fixedmeasurements_siteid_fkey FOREIGN KEY (siteid) REFERENCES admin.fixedsites(id);


--
-- Name: metadata_blockid; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_blockid FOREIGN KEY (blockid) REFERENCES admin.blocks(id);


--
-- Name: metadata_instrumentid; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_instrumentid FOREIGN KEY (instrumentid) REFERENCES admin.instruments(id);


--
-- Name: metadata_peopleid; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_peopleid FOREIGN KEY (peopleid) REFERENCES admin.people(id);


--
-- Name: mobiledata_sensor_fkey; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobiledata
    ADD CONSTRAINT mobiledata_sensor_fkey FOREIGN KEY (sensorid) REFERENCES admin.sensors(id);


--
-- Name: mobilemeasurements_blockid_fkey; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobilemeasurements
    ADD CONSTRAINT mobilemeasurements_blockid_fkey FOREIGN KEY (blockid) REFERENCES admin.blocks(id);


--
-- Name: mobilemeasurements_instrumentid; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobilemeasurements
    ADD CONSTRAINT mobilemeasurements_instrumentid FOREIGN KEY (instrumentid) REFERENCES admin.instruments(id);


--
-- Name: mobilemeasurements_peopleid_fkey; Type: FK CONSTRAINT; Schema: data; Owner: adm_penap
--

ALTER TABLE ONLY mobilemeasurements
    ADD CONSTRAINT mobilemeasurements_peopleid_fkey FOREIGN KEY (peopleid) REFERENCES admin.people(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin; Type: ACL; Schema: -; Owner: adm_penap
--

REVOKE ALL ON SCHEMA admin FROM PUBLIC;
REVOKE ALL ON SCHEMA admin FROM adm_penap;
GRANT ALL ON SCHEMA admin TO adm_penap;
GRANT USAGE ON SCHEMA admin TO datauser;


--
-- Name: data; Type: ACL; Schema: -; Owner: adm_penap
--

REVOKE ALL ON SCHEMA data FROM PUBLIC;
REVOKE ALL ON SCHEMA data FROM adm_penap;
GRANT ALL ON SCHEMA data TO adm_penap;
GRANT USAGE ON SCHEMA data TO datauser;


--
-- Name: gis_views; Type: ACL; Schema: -; Owner: adm_penap
--

REVOKE ALL ON SCHEMA gis_views FROM PUBLIC;
REVOKE ALL ON SCHEMA gis_views FROM adm_penap;
GRANT ALL ON SCHEMA gis_views TO adm_penap;
GRANT USAGE ON SCHEMA gis_views TO PUBLIC;
GRANT USAGE ON SCHEMA gis_views TO datauser;


--
-- Name: metadata; Type: ACL; Schema: -; Owner: adm_penap
--

REVOKE ALL ON SCHEMA metadata FROM PUBLIC;
REVOKE ALL ON SCHEMA metadata FROM adm_penap;
GRANT ALL ON SCHEMA metadata TO adm_penap;
GRANT USAGE ON SCHEMA metadata TO datauser;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
GRANT USAGE ON SCHEMA public TO datauser;


SET search_path = admin, pg_catalog;

--
-- Name: blocks; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE blocks FROM PUBLIC;
REVOKE ALL ON TABLE blocks FROM adm_penap;
GRANT ALL ON TABLE blocks TO adm_penap;
GRANT SELECT,INSERT,UPDATE ON TABLE blocks TO PUBLIC;
GRANT SELECT ON TABLE blocks TO datauser;


--
-- Name: fixedsites; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE fixedsites FROM PUBLIC;
REVOKE ALL ON TABLE fixedsites FROM adm_penap;
GRANT ALL ON TABLE fixedsites TO adm_penap;
GRANT SELECT ON TABLE fixedsites TO datauser;


--
-- Name: institutions; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE institutions FROM PUBLIC;
REVOKE ALL ON TABLE institutions FROM adm_penap;
GRANT ALL ON TABLE institutions TO adm_penap;
GRANT SELECT ON TABLE institutions TO datauser;


--
-- Name: instruments; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE instruments FROM PUBLIC;
REVOKE ALL ON TABLE instruments FROM adm_penap;
GRANT ALL ON TABLE instruments TO adm_penap;
GRANT SELECT ON TABLE instruments TO datauser;


--
-- Name: people; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE people FROM PUBLIC;
REVOKE ALL ON TABLE people FROM adm_penap;
GRANT ALL ON TABLE people TO adm_penap;
GRANT SELECT ON TABLE people TO datauser;


--
-- Name: sensors; Type: ACL; Schema: admin; Owner: adm_penap
--

REVOKE ALL ON TABLE sensors FROM PUBLIC;
REVOKE ALL ON TABLE sensors FROM adm_penap;
GRANT ALL ON TABLE sensors TO adm_penap;
GRANT SELECT ON TABLE sensors TO datauser;


SET search_path = data, pg_catalog;

--
-- Name: fixedmeasurements; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON TABLE fixedmeasurements FROM PUBLIC;
REVOKE ALL ON TABLE fixedmeasurements FROM adm_penap;
GRANT ALL ON TABLE fixedmeasurements TO adm_penap;
GRANT SELECT,INSERT,UPDATE ON TABLE fixedmeasurements TO PUBLIC;
GRANT SELECT ON TABLE fixedmeasurements TO datauser;


--
-- Name: fixedmeasurements_id_seq; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON SEQUENCE fixedmeasurements_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE fixedmeasurements_id_seq FROM adm_penap;
GRANT ALL ON SEQUENCE fixedmeasurements_id_seq TO adm_penap;
GRANT ALL ON SEQUENCE fixedmeasurements_id_seq TO PUBLIC;


--
-- Name: metadata; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON TABLE metadata FROM PUBLIC;
REVOKE ALL ON TABLE metadata FROM adm_penap;
GRANT ALL ON TABLE metadata TO adm_penap;
GRANT SELECT,INSERT,UPDATE ON TABLE metadata TO PUBLIC;
GRANT SELECT,INSERT,UPDATE ON TABLE metadata TO datauser;


--
-- Name: metadata_id_seq; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON SEQUENCE metadata_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE metadata_id_seq FROM adm_penap;
GRANT ALL ON SEQUENCE metadata_id_seq TO adm_penap;
GRANT ALL ON SEQUENCE metadata_id_seq TO PUBLIC;


--
-- Name: mobiledata; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON TABLE mobiledata FROM PUBLIC;
REVOKE ALL ON TABLE mobiledata FROM adm_penap;
GRANT ALL ON TABLE mobiledata TO adm_penap;
GRANT SELECT,INSERT,UPDATE ON TABLE mobiledata TO PUBLIC;
GRANT SELECT,INSERT,UPDATE ON TABLE mobiledata TO datauser;


--
-- Name: mobiledata_id_seq; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON SEQUENCE mobiledata_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mobiledata_id_seq FROM adm_penap;
GRANT ALL ON SEQUENCE mobiledata_id_seq TO adm_penap;
GRANT ALL ON SEQUENCE mobiledata_id_seq TO PUBLIC;


--
-- Name: mobilemeasurements; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON TABLE mobilemeasurements FROM PUBLIC;
REVOKE ALL ON TABLE mobilemeasurements FROM adm_penap;
GRANT ALL ON TABLE mobilemeasurements TO adm_penap;
GRANT SELECT,INSERT,UPDATE ON TABLE mobilemeasurements TO PUBLIC;
GRANT SELECT,INSERT,UPDATE ON TABLE mobilemeasurements TO datauser;


--
-- Name: mobilemeasurements_id_seq; Type: ACL; Schema: data; Owner: adm_penap
--

REVOKE ALL ON SEQUENCE mobilemeasurements_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mobilemeasurements_id_seq FROM adm_penap;
GRANT ALL ON SEQUENCE mobilemeasurements_id_seq TO adm_penap;
GRANT ALL ON SEQUENCE mobilemeasurements_id_seq TO PUBLIC;


--
-- PostgreSQL database dump complete
--

