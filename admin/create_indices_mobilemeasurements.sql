﻿-- Index: data.mobilemeasurements_blockid_idx

-- DROP INDEX data.mobilemeasurements_blockid_idx;

CREATE INDEX mobilemeasurements_blockid_idx
ON data.mobilemeasurements
USING btree
(blockid);

-- Index: data.mobilemeasurements_geom_gix

-- DROP INDEX data.mobilemeasurements_geom_gix;

CREATE INDEX mobilemeasurements_geom_gix
ON data.mobilemeasurements
USING gist
(geom);

-- Index: data.mobilemeasurements_gpsdate_idx

-- DROP INDEX data.mobilemeasurements_gpsdate_idx;

CREATE INDEX mobilemeasurements_gpsdate_idx
ON data.mobilemeasurements
USING btree
(gpsdate);

-- Index: data.mobilemeasurements_instrumentid_idx

-- DROP INDEX data.mobilemeasurements_instrumentid_idx;

CREATE INDEX mobilemeasurements_instrumentid_idx
ON data.mobilemeasurements
USING btree
(instrumentid);

-- Index: data.mobilemeasurements_peopleid_idx

-- DROP INDEX data.mobilemeasurements_peopleid_idx;

CREATE INDEX mobilemeasurements_peopleid_idx
ON data.mobilemeasurements
USING btree
(peopleid);

