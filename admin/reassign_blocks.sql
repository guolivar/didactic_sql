-- Get the right record
-- Select by day on timestamp
-- select by person and block name
-- change to the "OP" version of the block
-- Queen Street (3) to OP Queen Street (10)
-- Lower Queen Street (8) to OP Lower Queen Street (12)
-- Customs Street (9) to OP Customs Street (11)

UPDATE data.mobilemeasurements dm
    SET blockid = 10 --set the block to Out of Phase Queen Street
  WHERE dm.id in (select id from data.mobilemeasurements dmo
    where (dmo.recordtime,dmo.recordtime) OVERLAPS (timestamptz '2013-09-19 11:00 NZST',timestamptz '2013-09-19 15:00 NZST') -- the record time is within the noted run
    AND dmo.peopleid=(select id from admin.people where name='Nick') --the operator matches the metadata record
    );
    
    
    
    
