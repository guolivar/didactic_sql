select count(sd2.value) as nrecs, adi2.serialn as instrument
from data.mobiledata sd2,admin.sensors ads2, admin.instruments adi2
  where ads2.instrumentid=adi2.id
  and sd2.sensorid=ads2.id
  and sd2.flagid=2
  and ads2.name='CO'
group by adi2.id
order by count(sd2.value);

