select mm.geom, avg(md.value)
from data.mobilemeasurements as mm,
data.mobiledata as md,
admin.blocks as b,
admin.instruments as i,
admin.sensors as s
where
abs(md.sensortime-mm.gpstime)<0.0001 and
b.name='Britomart' and
b.id=mm.blockid and
s.id=md.sensorid and
s.instrumentid=i.id and
i.name='PTrak' and
s.name='N20'
group by
mm.geom;