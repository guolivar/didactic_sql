-- Get a table of <date>,<PTrak PNC> of all the points within 50m of the intersection
-- of Queen and Victoria streets.
-- note the exact match of the timestamp (octave format)
SELECT distinct mm.recordtime at time zone 'NZST' as recordtime, md.value N20, mm.geom
FROM data.mobilemeasurements mm,
  admin.sensors s,
  admin.instruments i,
  data.mobiledata md,
  (SELECT smm.gpsdate
  FROM admin.instruments si, admin.sensors ss, admin.blocks sb, data.mobilemeasurements smm, data.mobiledata smd
  WHERE si.name='GPS'
  AND ss.instrumentid=si.id
  AND ss.name='SPEED'
  AND smm.instrumentid=si.id
  AND smd.sensorid=ss.id
  AND smd.sensortime=smm.gpsdate
  -- speed less than X (m/s)
  AND smd.value <= 0.5) stops
WHERE mm.instrumentid = i.id
AND i.name = 'PTrak'::text
AND s.instrumentid = i.id
AND md.sensorid = s.id
AND s.name = 'N20'::text
AND md.sensortime = mm.gpsdate
AND ST_DWithin(mm.geom,ST_GeomFromEWKT('SRID=4326;POINT(174.76465 -36.84904)'),0.0005)
AND stops.gpsdate=mm.gpsdate;
--This location refers to ~within 50m of the intersection between Queen and Victoria streets


