-- Summary of the measurements taken, grouped by Route and selected both
-- by Route LABEL and position within X metres of the actual route
-- summary functions are average, min, max, stdeviation and count
-----------------------------------------
-- First select the output. Summary functions and route name.
SELECT distinct b.name as route, avg(md.value) as mean, max(md.value) as max, min(md.value) as min, count(md.value) as count
-- Then identify whic tables to use to select the data.
-- The syntax is <table> <alias> and the alias is to simplify the calls below
FROM admin.instruments i, admin.sensors s, admin.blocks b, data.mobilemeasurements mm, data.mobiledata md
-- Now come the actual conditions to match the tables and select the data.
WHERE i.name='PTrak' -- Instrument name
  AND s.instrumentid=i.id -- Match the instruments and sensors tables
  AND s.name='N20' -- Sensor name
  AND mm.instrumentid=i.id -- Match the instrument with the measurement point
  AND md.sensorid=s.id -- Match the sensor with the measurement data
  AND md.recordtime=mm.recordtime -- Match the GPS time with the sensor time
  AND mm.blockid=b.id -- Match the block from the measurements with the route
  AND ST_Within(mm.geom, ST_Buffer((select geom from admin.blocks where name=b.name), 0.0005)) -- Select only points that are INSIDE a 50m buffer around the shape of the routes (as polygons)
-- Finally, group the results by the name of the route (i.e, only output one row per route)
group by b.name;
