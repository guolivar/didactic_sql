-- Get a table of <date>,<PTrak PNC> of all the points within 50m of the intersection
-- of Queen and Victoria streets.
-- note the exact match of the timestamp (octave format)
SELECT mm.recordtime at time zone 'NZST'as recordtime, md.value N20, md.flagid
FROM data.mobilemeasurements mm,
  admin.sensors s,
  admin.instruments i,
  data.mobiledata md
WHERE mm.instrumentid = i.id
AND i.name = 'Langan'
AND s.instrumentid = i.id
AND md.sensorid = s.id
AND s.name = 'CO'
AND md.recordtime = mm.recordtime
AND ST_DWithin(mm.geom,(select geom from admin.fixedsites where id=3),0.0005)
limit 1500;
--This location refers to ~within 50m of the intersection between Queen and Victoria streets