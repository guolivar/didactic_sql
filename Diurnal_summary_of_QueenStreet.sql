-- Summary of the measurements taken, grouped by hour of the day (NZST)
-- by Route LABEL and position within X metres of the actual route
-- summary functions are average, min, max, stdeviation and count
-----------------------------------------
-- First select the output. Summary functions and route name.
SELECT distinct extract(hour from recordtime at time zone 'NZST') as timeh, count(df.value) as nn6, avg(df.value::numeric) as avn6, max(df.value::numeric) as mxn6, min(df.value::numeric) as mnn6
-- Then identify whic tables to use to select the data.
-- The syntax is <table> <alias> and the alias is to simplify the calls below
FROM data.fixedmeasurements df
-- Now come the actual conditions to match the tables and select the data.
WHERE df.siteid=2 --Queen Street
and df.parameterid=408 --N6
and df.value!='NaN'
-- Finally, group the results by NZST hour (i.e, only output one row per hour)
group by timeh
order by timeh;

