SELECT mm.id, mm.gpsdate as date, md.value as SPD, mm.geom
FROM admin.instruments i, admin.sensors s, admin.blocks b, data.mobilemeasurements mm, data.mobiledata md
WHERE i.name='GPS'
AND s.instrumentid=i.id
AND s.name='SPEED'
AND mm.instrumentid=i.id
AND md.sensorid=s.id
AND md.sensortime=mm.gpsdate
-- speed less than X (m/s)
AND md.value <= 0.5;