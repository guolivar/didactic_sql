-- Correct CO scaling.
-- Record time (NZST)
-- Geometry to the output
select co.rt at time zone 'NZST' as rt, 100*avg(co.co) as co, avg(n.n20) as n20, max(co.geom) as geom
from
  -- First table is the CO data ASSIGNED to the Victoria Street loop and WITHIN a 50m buffer around that route
  (select comd.value as co, comd.recordtime rt, comm.geom geom
  from data.mobilemeasurements comm, data.mobiledata comd, admin.sensors cos, admin.instruments coi, admin.blocks cob
  where coi.name='Langan'
  AND cos.instrumentid=coi.id
  AND cos.name='CO'
  AND comm.instrumentid=coi.id
  AND comd.sensorid=cos.id
  AND comd.recordtime=comm.recordtime
  AND cob.id=comm.blockid
  AND cob.name='Victoria Street'
  AND ST_Within(comm.geom, ST_Buffer((select geom from admin.blocks where name=cob.name), 0.0005))) co,
  -- Second table is the N20 data ASSIGNED to the Victoria Street loop and WITHIN a 50m buffer around that route
  (select nmd.value as n20, nmd.recordtime rt
  from data.mobilemeasurements nmm, data.mobiledata nmd, admin.sensors ns, admin.instruments ni, admin.blocks nb
  where ni.name='PTrak'
  AND ns.instrumentid=ni.id
  AND ns.name='N20'
  AND nmm.instrumentid=ni.id
  AND nmd.sensorid=ns.id
  AND nmd.recordtime=nmm.recordtime
  AND nb.id=nmm.blockid
  AND nb.name='Victoria Street'
  AND ST_Within(nmm.geom, ST_Buffer((select geom from admin.blocks where name=nb.name), 0.0005))) n
where
  -- The aggregation (average) is taken from the N20 data +/- 1 minute from the CO timestamp.
  n.rt>=co.rt- interval '1 minute'
  AND n.rt<= co.rt+ interval '1 minute'
group by co.rt;