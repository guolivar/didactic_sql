-- This is a VERY RESOURCE INTENSIVE query so expect it to take several minutes (maybe hours!)
-- Get all the datapoints with their associated route name
-- matched both by expected route and location within that route
-- Output is timestamp, measured value and route name.
-- md.geom would add a geometry column to use in a GIS
-- Get N20, CO, Temperature and GPS Speed for all of them
-- gouped by GPS date
-- The averaging is only an artifact because there can only be 1 matching record per
-- timestamp AND ROUTEID
SELECT DISTINCT gps.recordtime at time zone 'NZST' as recordtime, avg(n20.n20), avg(co.co), avg(tmp.temp), avg(gps.spd), max(b.name)
FROM
  (SELECT distinct nmd.recordtime at time zone 'NZST' as recordtime, nmd.value as n20, nb.name as route
  FROM admin.instruments ni, admin.sensors ns, admin.blocks nb, data.mobilemeasurements nmm, data.mobiledata nmd
  WHERE ni.name='PTrak' --The instrument to use
    AND ns.instrumentid=ni.id
    AND ns.name='N20' --The measurement to use
    AND nmm.instrumentid=ni.id
    AND nmd.sensorid=ns.id
    AND nmd.sensortime=nmm.gpsdate
    AND nmm.blockid=nb.id
    AND ST_Within(nmm.geom, ST_Buffer((select geom from admin.blocks where name=nb.name), 0.0005))) n20,
  (SELECT distinct comd.recordtime at time zone 'NZST' as recordtime, comd.value as co, cob.name as route
  FROM admin.instruments coi, admin.sensors cos, admin.blocks cob, data.mobilemeasurements comm, data.mobiledata comd
  WHERE coi.name='Langan' --The instrument to use
    AND cos.instrumentid=coi.id
    AND cos.name='CO' --The measurement to use
    AND comm.instrumentid=coi.id
    AND comd.sensorid=cos.id
    AND comd.sensortime=comm.gpsdate
    AND comm.blockid=cob.id
    AND ST_Within(comm.geom, ST_Buffer((select geom from admin.blocks where name=cob.name), 0.0005))) co,
  (SELECT distinct tmpmd.recordtime at time zone 'NZST' as recordtime, tmpmd.value as temp, tmpb.name as route
  FROM admin.instruments tmpi, admin.sensors tmps, admin.blocks tmpb, data.mobilemeasurements tmpmm, data.mobiledata tmpmd
  WHERE tmpi.name='Langan' --The instrument to use
    AND tmps.instrumentid=tmpi.id
    AND tmps.name='Temperature' --The measurement to use
    AND tmpmm.instrumentid=tmpi.id
    AND tmpmd.sensorid=tmps.id
    AND tmpmd.sensortime=tmpmm.gpsdate
    AND tmpmm.blockid=tmpb.id
    AND ST_Within(tmpmm.geom, ST_Buffer((select geom from admin.blocks where name=tmpb.name), 0.0005))) tmp,
  (SELECT distinct gpsmd.recordtime at time zone 'NZST' as recordtime, gpsmd.value as spd, gpsb.name as route
  FROM admin.instruments gpsi, admin.sensors gpss, admin.blocks gpsb, data.mobilemeasurements gpsmm, data.mobiledata gpsmd
  WHERE gpsi.name='GPS' --The instrument to use
    AND gpss.instrumentid=gpsi.id
    AND gpss.name='SPEED' --The measurement to use
    AND gpsmm.instrumentid=gpsi.id
    AND gpsmd.sensorid=gpss.id
    AND gpsmd.sensortime=gpsmm.gpsdate
    AND gpsmm.blockid=gpsb.id
    AND ST_Within(gpsmm.geom, ST_Buffer((select geom from admin.blocks where name=gpsb.name), 0.0005))) gps
WHERE gps.route=co.route
  AND gps.route=tmp.route
  AND gps.route=n20.route
GROUP BY gps.recordtime;